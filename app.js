const 	express 				= require('express');
const 	expressLayouts 			= require('express-ejs-layouts');
const 	mongoose 				= require('mongoose');
const 	flash 					= require('connect-flash');
const 	session 				= require('express-session');
const 	passport 				= require('passport');
const 	moment 					= require('moment');
const 	expressValidator 		= require('express-validator');
const 	app 					= express();
const 	cookieParser 			= require('cookie-parser');
const 	DSAuthCodeGrant 		= require('./lib/DSAuthCodeGrant')
const	DsJwtAuth 				= require('./lib/DSJwtAuth')
const	DSJwtAuthAdmin 			= require('./lib/DSJwtAuthAdmin')
const	DocusignStrategy 		= require('passport-docusign')
const	docOptions 				= require('./config/documentOptions.json')
const	docNames 				= require('./config/documentNames.json')
const	commonControllers 		= require('./lib/commonControllers')
const	commonAdminControllers 	= require('./lib/commonAdminControllers')
const	dsConfig 				= require('./config/index.js').config
const	csrf 					= require('csurf')
const	eg001 					= require('./eg001EmbeddedSigning');
const	eg001Admin 				= require('./eg001EmbeddedSigningAdmin');
const 	path 					= require('path');
const 	Jsontableify            = require('jsontableify')
const 	config					= require('./config/index.js').config;
//const bcrypt = require('bcryptjs');
//Passport Config
// require('./src/modules/config/passport')(passport);
const {	acceptSignRedirect, } 	= require('./src/modules/admin/application.controller');
const HOST = process.env.HOST,
	PORT = process.env.PORT || 5100;
let hostUrl = 'http://' + HOST + ':' + PORT;
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

if (dsConfig.appUrl != '' && dsConfig.appUrl != '{APP_URL}') {
	hostUrl = dsConfig.appUrl;
}
app.use(cookieParser());
const csrfProtection = csrf({
	cookie: true
});

//app.use(expressValidator())
const adminRoutes = require('./src/modules/admin/admin.routes');
const userRoutes = require('./src/modules/user/user.routes');
const {
	localsName
} = require('ejs');

//DB Config
const db = require('./src/modules/config/mongo').mongoURI;

//Connect to Mongo
mongoose.connect(db, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
});

mongoose.connection.on('error', () => {
	throw new Error(`unable to connect to database: ${db}`);
});

const documiddleware = (req, res, next) => {
	if (req.session.application && req.session.application.isDraft) {
		req.dsAuthCodeGrant = new DSAuthCodeGrant(req);
		req.dsAuthJwt = new DsJwtAuth(req);
		req.dsAuth = req.dsAuthCodeGrant;
		if (req.session.authMethod === 'jwt-auth') {
			req.dsAuth = req.dsAuthJwt;
		}
		next();
	} else {
		return res.redirect('/login');
	}
};
const docuAdminmiddleware = (req, res, next) => {
	if (req.session.admin) {
		req.dsAuthCodeGrant = new DSAuthCodeGrant(req);
		req.dsAuthJwt = new DSJwtAuthAdmin(req);
		req.dsAuth = req.dsAuthCodeGrant;
		if (req.session.authMethod === 'jwt-auth') {
			req.dsAuth = req.dsAuthJwt;
		}
		next();
	} else {
		console.log(req.session.admin);

		return res.redirect('/admin/login');
	}
};

app.use(function (req, res, next) {
	res.set(
		'Cache-Control',
		'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0'
	);
	next();
});

//EJS
//app.use(expressLayouts);
app.set('views', path.join(__dirname, '/src/modules/views'));
app.set('view engine', 'ejs');
app.use(express.static('./src/modules/public'));

//BodyParser
app.use(express.urlencoded({
	extended: true
}));

//Express Session
app.use(
	session({
		secret: 'secret',
		name: 'name',
		resave: true,
		saveUninitialized: true,
	})
);

//Passport middleware
app.use(passport.initialize());
app.use(
	passport.session({
		secret: 'secret',
		name: 'name',
		proxy: true,
		resave: true,
		saveUninitialized: true,
		resave: true,
	})
);

//Connect flash
app.use(flash());

//Global Vars
app.use((req, res, next) => {
	res.locals.admin = req.session.admin;
	res.locals.appurl = process.env.MEDIAURL;
	res.locals.moment = moment;
 	res.locals.Jsontableify = Jsontableify;
	res.locals.user = req.session.application;
	res.locals.success = req.flash('success');
	res.locals.error = req.flash('error');
	res.locals.config = config;
	res.locals.dsConfig = {
		...dsConfig,
		docOptions: docOptions,
		docNames: docNames,
	};
	next();
});

app.use('/admin', adminRoutes);
app.use(userRoutes);

///docusignRoutes
app
	// .get('/', documiddleware, commonControllers.indexController)
	.get('/ds/login', documiddleware, commonControllers.login)
	.get('/ds/callback', documiddleware, [dsLoginCB1, dsLoginCB2]) // OAuth callbacks. See below
	.get('/ds/logout', documiddleware, commonControllers.logout)
	.get('/ds/logoutCallback', documiddleware, commonControllers.logoutCallback)
	.get(
		'/ds/mustAuthenticate',
		documiddleware,
		commonControllers.mustAuthenticateController
	)
	.get('/ds-return', documiddleware, commonControllers.returnController)
	.get('/admin/ds/login', docuAdminmiddleware, commonAdminControllers.login)
	.get('/admin/ds/callback', docuAdminmiddleware, [dsLoginCB1, dsLoginCB2]) // OAuth callbacks. See below
	.get('/admin/ds/logout', docuAdminmiddleware, commonAdminControllers.logout)
	.get(
		'/admin/ds/logoutCallback',
		docuAdminmiddleware,
		commonAdminControllers.logoutCallback
	)
	.get(
		'/admin/ds/mustAuthenticate',
		docuAdminmiddleware,
		commonAdminControllers.mustAuthenticateController
	)
	.get('/admin/ds-return', docuAdminmiddleware, acceptSignRedirect)
	.use(csrfProtection); // CSRF protection for the following routes
if (dsConfig.examplesApi !== 'rooms') {
	app
		.get('/docusign', documiddleware, eg001.getController)
		.post('/docusign', documiddleware, eg001.createController);

	app
		.get('/admin/docusign', docuAdminmiddleware, eg001Admin.getController)
		.post('/admin/docusign', docuAdminmiddleware, eg001Admin.createController);
}

function dsLoginCB1(req, res, next) {
	req.dsAuthCodeGrant.oauth_callback1(req, res, next);
}

function dsLoginCB2(req, res, next) {
	req.dsAuthCodeGrant.oauth_callback2(req, res, next);
}

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete DocuSign profile is serialized
//   and deserialized.
passport.serializeUser(function (user, done) {
	done(null, user);
});
passport.deserializeUser(function (obj, done) {
	done(null, obj);
});

let scope = 'signature';
if (dsConfig.examplesApi === 'rooms') {
	scope +=
		' dtr.rooms.read dtr.rooms.write dtr.documents.read dtr.documents.write dtr.profile.read dtr.profile.write dtr.company.read dtr.company.write room_forms';
}

// Configure passport for DocusignStrategy
let docusignStrategy = new DocusignStrategy({
		production: dsConfig.production,
		clientID: dsConfig.dsClientId,
		scope: scope,
		clientSecret: dsConfig.dsClientSecret,
		callbackURL: hostUrl + '/ds/callback',
		state: true, // automatic CSRF protection.
		// See https://github.com/jaredhanson/passport-oauth2/blob/master/lib/state/session.js
	},
	function _processDsResult(accessToken, refreshToken, params, profile, done) {
		// The params arg will be passed additional parameters of the grant.
		// See https://github.com/jaredhanson/passport-oauth2/pull/84
		//
		// Here we're just assigning the tokens to the account object
		// We store the data in DSAuthCodeGrant.getDefaultAccountInfo
		let user = profile;
		user.accessToken = accessToken;
		user.refreshToken = refreshToken;
		user.expiresIn = params.expires_in;
		user.tokenExpirationTimestamp = moment().add(user.expiresIn, 's'); // The dateTime when the access token will expire
		return done(null, user);
	}
);

let docusignAdminStrategy = new DocusignStrategy({
		production: dsConfig.production,
		clientID: dsConfig.dsClientId,
		scope: scope,
		clientSecret: dsConfig.dsClientSecret,
		callbackURL: hostUrl + '/admin/ds/callback',
		state: true, // automatic CSRF protection.
		// See https://github.com/jaredhanson/passport-oauth2/blob/master/lib/state/session.js
	},
	function _processDsResult(accessToken, refreshToken, params, profile, done) {
		// The params arg will be passed additional parameters of the grant.
		// See https://github.com/jaredhanson/passport-oauth2/pull/84
		//
		// Here we're just assigning the tokens to the account object
		// We store the data in DSAuthCodeGrant.getDefaultAccountInfo
		let user = profile;
		user.accessToken = accessToken;
		user.refreshToken = refreshToken;
		user.expiresIn = params.expires_in;
		user.tokenExpirationTimestamp = moment().add(user.expiresIn, 's'); // The dateTime when the access token will expire
		return done(null, user);
	}
);

/**
 * The DocuSign OAuth default is to allow silent authentication.
 * An additional OAuth query parameter is used to not allow silent authentication
 */
if (!dsConfig.allowSilentAuthentication) {
	// See https://stackoverflow.com/a/32877712/64904
	docusignStrategy.authorizationParams = function (options) {
		return {
			prompt: 'login'
		};
	};
}

passport.use(docusignStrategy);

app.listen(PORT, console.log(`Server Started On Port ${PORT}`));
