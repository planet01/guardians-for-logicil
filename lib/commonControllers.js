/**
 * @file
 * This file provides common controllers.
 * @author DocuSign
 */

const fs = require('fs'),
  dsConfig = require('../config/appsettings.json'),
  User = require('../src/modules/models/user/user'),
  docusign = require('docusign-esign'),
  documentationTopic = 'auth-code-grant-node';
const commonControllers = exports;

const path = require('path');

/**
 * Home page for this application
 */
commonControllers.indexController = (req, res) => {
  if (dsConfig.quickstart == 'true' && req.user == undefined) {
    console.debug('quickstart mode on');
    return res.redirect('/docusign');
  } else {
    res.render('pages/index', {
      title: 'Home',
      documentation: dsConfig.documentation + documentationTopic,
      showDoc: dsConfig.documentation,
    });
  }
};

commonControllers.mustAuthenticateController = (req, res) => {
  if (dsConfig.quickstart == 'true') res.redirect('login');
  else return res.redirect('/ds/login');
  // res.render('pages/ds_must_authenticate', {
  //   title: 'Authenticate with DocuSign',
  // });
};

commonControllers.login = (req, res, next) => {
  // const { auth } = req.query;
  // if (auth === 'grand-auth') {
  //   req.dsAuth = req.dsAuthCodeGrant;
  // } else if (auth === 'jwt-auth') {
  //   req.dsAuth = req.dsAuthJwt;
  // }
  req.dsAuth = req.dsAuthJwt;

  req.dsAuth.login(req, res, next);
};

commonControllers.logout = (req, res) => {
  dsConfig.quickstart = 'false';
  req.dsAuth.logout(req, res);
};

commonControllers.logoutCallback = (req, res) => {
  req.dsAuth.logoutCallback(req, res);
};

/**
 * Display parameters after DS redirect to the application
 * after an embedded signing ceremony, etc
 * @param {object} req Request object
 * @param {object} res Result object
 */
commonControllers.returnController = async (req, res) => {
  try {
    const updateUser = await User.findByIdAndUpdate(
      req.session.application._id,
      { isDraft: false },
      {
        upsert: true,
        new: true,
      }
    );
    req.session.application = updateUser;

    let dsApiClient = new docusign.ApiClient();
    dsApiClient.setBasePath(req.session.basePath);
    dsApiClient.addDefaultHeader(
      'Authorization',
      'Bearer ' + req.user.accessToken
    );
    let envelopesApi = new docusign.EnvelopesApi(dsApiClient),
      results = null;

    // Step 1. EnvelopeDocuments::get.
    // Exceptions will be caught by the calling function
    results = await envelopesApi.getDocument(
      req.session.accountId,
      req.query.envelopeId,
      '3',
      null
    );

    var filename = 'ws' + req.session.application._id + '.pdf';
    fs.writeFile(
      'src/modules/public/users/pdf/' + filename,
      results,
      'binary',
      function (err, data) {
        if (err) return console.log(err);
      }
    );
    // res.render('/register-detail', {
    //   title: 'Return from DocuSign',
    //   event: event,
    //   envelopeId: envelopeId,
    //   state: state,
    // });
  } catch (err) {
    console.log(err);
  }

  res.redirect('/register-detail');
};
