const NodeCache = require("node-cache");
const myCache = new NodeCache();

const setCache = (key, value) => myCache.set(key, value);

const getCache = (key) => myCache.get(key);

module.exports = { getCache, setCache };