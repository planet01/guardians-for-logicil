const User = require('../models/user/user');
const Audit = require('../models/common/audit');

exports.getAudit = async (req, res) => {
  try {
    const audits = await Audit.find({})
      .populate({ path: 'userId', select: 'email firstName lastName' })
      .populate({ path: 'rejectUserId', select: 'email firstName lastName' })
      .populate({ path: 'applicationStatus.admin', select: 'email username' })
      .populate({ path: 'draftEmail',
        populate: {
          path: 'emailSend',
        },
      })
      .lean();
    // console.log(audits[0].applicationStatus)
    console.log(audits)
    res.render('admin/audit', { audits });
  } catch (err) {
    console.log(err);
  }
};
