const Configuration = require('../models/admin/configuration');

exports.getEmailCredentials = async (req, res) => {
  try {
    const configuration = await Configuration.findOne(
      {},
      'service host port hostemail hostpassword'
    ).lean();
    res.render('admin/email-credentials', { configuration });
  } catch (err) {
    console.log(err);
  }
};

exports.postEmailCredentials = async (req, res) => {
  try {
    // req.body.service = req.body.smtp_host;
    // req.body.hostemail = req.body.email;
    // req.body.hostpassword = req.body.password;
    // req.body.host = req.body.host_service;

    const configuration = await Configuration.findByIdAndUpdate(
      req.body.id,
      {
        service: req.body.host_service,
        port: req.body.smtp_port,
        hostemail: req.body.email,
        hostpassword: req.body.password,
        host: req.body.smtp_host,
      },
      { new: true }
    );
    req.flash('success', 'Email Credentials Updated Successfully');
    res.render('admin/email-credentials', { configuration });
  } catch (err) {
    console.log(err);
  }
};

exports.acceptApplication = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id, {
      isApprove: true,
    });
    return res.redirect('/admin/applications');
  } catch (err) {
    console.log(err);
  }
};

exports.rejectApplication = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id, {
      isApprove: false,
    });
    return res.redirect('/admin/applications');
  } catch (err) {
    console.log(err);
  }
};
