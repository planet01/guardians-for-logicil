const AccountOpenignAPIHandler 			= require('../services/accountOpeningAPIService.js');
const { setCache, getCache }        = require('../../../lib/utilities/cache');
const { db } = require('../models/user/user');
const User 			                    = require('../models/user/user');
const configs                       = require('../../../config/index.js').config;
const moment                        = require("moment");

const Login = async () => {
    if(!getCache("accountOpeningAccessToken") || new moment(getCache("accountOpeningTokenTimeout"), 'MMDDyyyy HHmmss') <= new moment()){
      const response = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret);
        var expires = new moment().add(response.data.expires_in - 60, 'seconds');
        setCache("accountOpeningAccessToken", response.data.access_token);
        setCache("accountOpeningRSAPublicKey", response.data.publicKey);
        setCache("accountOpeningTokenTimeout", expires.format("MMDDyyyy HHmmss"));
    }

    //  setCache("accountOpeningAccessToken", '');
    //  setCache("accountOpeningRSAPublicKey", '-----BEGIN PUBLIC KEY-----\r\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCobtNh/OJAuP1t9wnd2CEVfZ5x\r\nAG8G91dtC4bCLVrBjzdUhNsINTvF0C0vblijzUbCi+o++mFezlnuljcfLsS5adxy\r\nw/oX9nL89bOprC5mlgmn8sTEMc5ceF0HqO/iPwEVbQ/CLSYC1ucQYYVBqbNe8r4/\r\n2akAjplYZmpuB1gScQIDAQAB\r\n-----END PUBLIC KEY-----\r\n');
}

exports.callAccountOpenignAPI = async (req, res) => {
  try {
      await Login();
      //const response = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret);
      //setCache("accountOpeningAccessToken", response.data.access_token);
      //setCache("accountOpeningRSAPublicKey", response.data.publicKey);
  
      const user = await User.findById(req).lean();
      var res = await AccountOpenignAPIHandler.setupUser(user);
      await AccountOpenignAPIHandler.otherData(user, res.data);
      console.log("UserIdentifierCode", res.data);

      await User.updateOne({ _id: user._id }, { userIdentificationCode: res.data, isDraft: false }, function(err, res){
          if(err){
            console.log(err);
          }
          else{
          }
      });

      return true;
  } catch (err) {
    printErrorMessages('callAccountOpenignAPI', err);
    return false;
  }
};

exports.iClearAccountOpening = async (req, res) => {
  try {
    await Login();

    const user = await User.findById(req).lean();
    var res = await AccountOpenignAPIHandler.openIClearAccount(user, ['M', 'C']);
    // res = await AccountOpenignAPIHandler.openIClearAccount(user, 'C');
    return true;
  } catch (err) {
    printErrorMessages('iClearAccountOpening', err);
    return false;
  }
}

exports.getBridgerResponseDetail = async (req, res) => {
  try{
    await Login();
    const user = await User.findOne({ _id: req.params.id }).lean();
  
    let userBridgerParams = {
      "userIdentifierCode": user.userIdentificationCode,
      "getOFACResponses": true
    };
    
    //res.render('admin/bridger-response', { data: sampleResponse });
  
    if (user.userIdentificationCode) {
      const bridgerResponse = await AccountOpenignAPIHandler.getBridgerResponse(userBridgerParams);
      var isArray = Object.prototype.toString.call(bridgerResponse.data) === '[object Array]';
      if(isArray){
        res.render('admin/bridger-response', { data: bridgerResponse.data, user: user });        
      }
      else{
        res.render('admin/bridger-response', { data: bridgerResponse.data, user: user });
      }
    }
    else {
      res.render('admin/bridger-response', { data: [], user: user });
    }
  }
  catch(err){
    printErrorMessages('getBridgerResponseDetail', err);
  }
}

exports.getTrueIdResponseDetail = async (req, res) => {
  try {
    await Login();
    
    const user = await User.findOne({ _id: req.params.id }).lean();
    //get userIdentifierCode from userDetail
    let userTrueIdParams = {
      "userIdentifierCode": user.userIdentificationCode
    };

    //res.render('admin/trueId-response', { data: sampleResponse });
    if (user.userIdentificationCode) {
      const trueIdResponse = await AccountOpenignAPIHandler.getTrueIdResponse(userTrueIdParams);
      res.render('admin/trueId-response', { data: trueIdResponse.data, user: user });
    }
    else {
      res.render('admin/trueId-response', { data: [], user: user });
    }
  }
  catch (err) {
    printErrorMessages('getTrueIdResponseDetail', err);
  }
}

exports.rerunBridger = async(req, res) => {
  try {
    await Login();
    const user = await User.findOne({ _id: req.params.id }).lean();
    let params = {
      "searchParam": user.userIdentificationCode,
      "actSearchParamAsBackOfficeAccountNumber": false
    };
    
  
    if (req.params.id) {
      const bridgerResponse = await AccountOpenignAPIHandler.rerunBridger(params);
      res.render('admin/bridger-response', { data: bridgerResponse.data, user: user });
    }
    else {
      res.render('admin/bridger-response', { data: [], user: user });
    }
  }
  catch (err) {
    printErrorMessages('rerunBridger', err);
  }
}

exports.rerunTrueID = async(req, res) => {
  try {
    await Login();
    const user = await User.findOne({ _id: req.params.id }).lean();
    let params = {
      "searchParam": user.userIdentificationCode,
      "actSearchParamAsBackOfficeAccountNumber": false
    };
    
  
    if (req.params.id) {
      const trueIDResponse = await AccountOpenignAPIHandler.rerunTrueID(params);
      res.render('admin/trueId-response', { data: trueIDResponse.data, user: user });
    }
    else {
      res.render('admin/trueId-response', { data: [], user: user });
    }
  }
  catch (err) {
    printErrorMessages('rerunTrueID', err);
  }
}

const printErrorMessages = (key, error) => {
  console.log(key, error);
  if(error && error.response){
    if (error.response.status == 401)
    console.log(key, "Please login first.");
    else if (error.response.status == 400) {
        console.log(key, error.response.data);
    }
    else {
        console.log(key, error);
    }
  }
}

const isArray = (data) => {
  return Object.prototype.toString.call(data) === '[object Array]'
}