$(function () {
  $('#mailAddother').on('click', function () {
    if ($(this).prop('checked')) {
      $('.mail-address').show();
    } else {
      $('.mail-address').hide();
    }
  });
});

$(function () {
  $('#trustedPerson').on('click', function () {
    if ($(this).prop('checked')) {
      $('.trusted_person').show();
    } else {
      $('.trusted_person').hide();
    }
  });
});

(function (document) {
  'use strict';

  var LightTableFilter = (function (Arr) {
    var _input;

    function _onInputEvent(e) {
      _input = e.target;
      var tables = document.getElementsByClassName(
        _input.getAttribute('data-table')
      );
      Arr.forEach.call(tables, function (table) {
        Arr.forEach.call(table.tBodies, function (tbody) {
          Arr.forEach.call(tbody.rows, _filter);
        });
      });
    }

    function _filter(row) {
      var text = row.textContent.toLowerCase(),
        val = _input.value.toLowerCase();
      row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
    }

    return {
      init: function () {
        var inputs = document.getElementsByClassName('light-table-filter');
        Arr.forEach.call(inputs, function (input) {
          input.oninput = _onInputEvent;
        });
      },
    };
  })(Array.prototype);

  document.addEventListener('readystatechange', function () {
    if (document.readyState === 'complete') {
      LightTableFilter.init();
    }
  });
})(document);

$(document).ready(function () {
  // Add minus icon for collapse element which is open by default
  $('.collapse.show').each(function () {
    $(this)
      .prev('.card-header')
      .find('.fa')
      .addClass('fa-minus')
      .removeClass('fa-plus');
  });

  // Toggle plus minus icon on show hide of collapse element
  $('.collapse')
    .on('show.bs.collapse', function () {
      $(this)
        .prev('.card-header')
        .find('.fa')
        .removeClass('fa-plus')
        .addClass('fa-minus');
    })
    .on('hide.bs.collapse', function () {
      $(this)
        .prev('.card-header')
        .find('.fa')
        .removeClass('fa-minus')
        .addClass('fa-plus');
    });

  if ($(window).width() <= 575) {
    $('.dblogo,.dbnav,.dbadmin').wrapAll(
      '<div id="mobileNav" class="sidenav"></div>'
    );
    $('.sidenav').append(
      "<a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>"
    );
  } else {
    $('.dblogo,.dbnav,.dbadmin').unwrap(
      '<div id="mobileNav" class="sidenav"></div>'
    );
  }
});

// $('.next').click(function(){
//    $(this).parent().hide().next().show();//hide parent and show next
// });

// $('.next').click(function(){
//      $(this).parent().hide().next().show();
// });
var left, opacity, scale; //fieldset properties which we will animate
var animating;

$('.stepbox-1').submit(function (ev) {
  alert('asd');
  ev.preventDefault();
  var form = $('.stepbox-1');
  if (form.valid() === false) {
    ev.stopPropagation();
  }
});

function stepBox1() {
  var form = $('.stepbox-1');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (form.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail1',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox1-btn').prop('disabled', true);
      },
      success: function (response, xhr) {
        if (response.success == 1) {
          $('.stepbox-2').show();
          $('.stepbox-1').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-2')))
            .addClass('active');
          $('#stepbox1-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );

          //   if(animating) return false;
          //   animating = true;
          //   form.animate({opacity: 0}, {
          //     step: function(now, mx) {
          //         //as the opacity of current_fs reduces to 0 - stored in "now"
          //         //1. scale current_fs down to 80%
          //         scale = 1 - (1 - now) * 0.2;
          //         //2. bring next_fs from the right(50%)
          //         left = (now * 50)+"%";
          //         //3. increase opacity of next_fs to 1 as it moves in
          //         opacity = 1 - now;
          //         form.css({
          //     'transform': 'scale('+scale+')',
          //     'position': 'relative',
          //     'top'     : '0'
          //   });
          //   $('.stepbox-2').css({'left': left, 'opacity': opacity});
          //     },
          //     duration: 500,
          //     complete: function(){
          //         form.hide();
          //         animating = false;
          //     },
          //     //this comes from the custom easing plugin
          //     easing: 'easeInQuad'
          // });
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
    //}
  } else {
    $('[data-toggle="tooltip"]').tooltip('open');
  }
  form.addClass('was-validated');
}

function stepBox2() {
  var form = $('.stepbox-2');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail2',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox2-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-3').show();
          $('.stepbox-2').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-3')))
            .addClass('active');
          $('#stepbox2-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox3() {
  var form = $('.stepbox-3');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail3',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox3-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-4').show();
          $('.stepbox-3').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-4')))
            .addClass('active');
          $('#stepbox3-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox4() {
  var form = $('.stepbox-4');

  let flag = 0;
  if (
    $('#annualIncome:checked').val() == undefined ||
    $('#liquidNetWorth:checked').val() == undefined ||
    $('#netWorth:checked').val() == undefined ||
    $('#taxRate:checked').val() == undefined ||
    $('#liquidNetWorth:checked').data('value') >
      $('#netWorth:checked').data('value')
  ) {
    flag = 0;
  } else {
    $('.annualIncome-error').html('');
    $('.liquidNetWorth-error').html('');
    $('.label-icon')
      .find('liquidNetWorth_error')
      .removeClass('liquidNetWorth_error');
    $('.netWorth-error').html('');
    $('.taxRate-error').html('');

    flag = 1;
  }

  if (flag == 1) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail4',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox4-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-5').show();
          $('.stepbox-4').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-5')))
            .addClass('active');
          $('#stepbox4-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  } else {
    if ($('#annualIncome:checked').val() == undefined) {
      $('.annualIncome-error').html(
        '<span style="color:red"> Annual Income is required </span>'
      );
    } else {
      $('.annualIncome-error').html('');
    }
    if ($('#liquidNetWorth:checked').val() == undefined) {
      $('.liquidNetWorth-error').html(
        '<span style="color:red"> Liquid net worth is required </span>'
      );
    } else {
      $('.liquidNetWorth-error').html('');
    }
    if ($('#netWorth:checked').val() == undefined) {
      $('.netWorth-error').html(
        '<span style="color:red"> Net worth is required </span>'
      );
    } else {
      $('.netWorth-error').html('');
    }
    if ($('#taxRate:checked').val() == undefined) {
      $('.taxRate-error').html(
        '<span style="color:red"> Net worth is required </span>'
      );
    } else {
      $('.taxRate-error').html('');
    }
    if (
      $('#netWorth:checked').val() !== undefined &&
      $('#liquidNetWorth:checked').val() !== undefined &&
      $('#liquidNetWorth:checked').data('value') >
        $('#netWorth:checked').data('value')
    ) {
      $('#liquidNetWorth:checked')
        .parents('label-icon')
        .addClass('liquidNetWorth_error');

      $('.liquidNetWorth-error').html(
        '<span style="color:red"> Liquid net worth should not be greater than net worth </span>'
      );
    } else {
      $('div[style="border: 2px solid red;"]');
    }
  }

  form.addClass('was-validated');
}

function stepBox5() {
  var form = $('.stepbox-5');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail5',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox5-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-6').show();
          $('.stepbox-5').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-6')))
            .addClass('active');
          $('#stepbox5-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox6() {
  var form = $('.stepbox-6');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail6',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox6-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-7').show();
          $('.stepbox-6').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-7')))
            .addClass('active');
          $('#stepbox6-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  } else {
    if ($('#significantRisk').hasClass('error')) {
      $('label[for="significantRisk"]').addClass('error-new');
    } else {
      $('label[for="significantRisk"]').removeClass('error-new');
    }
  }
  form.addClass('was-validated');
}

function stepBox7() {
  var form = $('.stepbox-7');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  let flag = 0;
  if (
    $('#annualExpense:checked').val() == undefined ||
    $('#specialExpense:checked').val() == undefined ||
    $('#liquidityExpense:checked').val() == undefined ||
    $('#financialGoal:checked').val() == undefined
  ) {
    flag = 0;
  } else {
    $('.annualExpense-error').html('');
    $('.specialExpense-error').html('');

    $('.liquidityExpense-error').html('');
    $('.financialGoal-error').html('');

    flag = 1;
  }
  if (flag == 1) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail7',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox7-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-8').show();
          $('.stepbox-7').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-8')))
            .addClass('active');
          $('#stepbox7-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  } else {
    if ($('#annualExpense:checked').val() == undefined) {
      $('.annualExpense-error').html(
        '<span style="color:red"> Annual Expense is required </span>'
      );
    } else {
      $('.annualExpense-error').html('');
    }
    if ($('#specialExpense:checked').val() == undefined) {
      $('.specialExpense-error').html(
        '<span style="color:red"> Special Expense worth is required </span>'
      );
    } else {
      $('.specialExpense-error').html('');
    }
    if ($('#liquidityExpense:checked').val() == undefined) {
      $('.liquidityExpense-error').html(
        '<span style="color:red"> Liquidity Expense is required </span>'
      );
    } else {
      $('.liquidityExpense-error').html('');
    }
    if ($('#financialGoal:checked').val() == undefined) {
      $('.financialGoal-error').html(
        '<span style="color:red"> Financial Goal is required </span>'
      );
    } else {
      $('.financialGoal-error').html('');
    }
  }
  form.addClass('was-validated');
}

function stepBox8() {
  var form = $('.stepbox-8');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail8',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox8-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-9').show();
          $('.stepbox-8').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-9')))
            .addClass('active');
          $('#stepbox8-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox9() {
  var form = $('.stepbox-9');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = new FormData(form[0]);
    $.ajax({
      url: '/register-detail9',
      method: 'post',
      data: formSerialize,
      cache: false,
      contentType: false,
      processData: false,
      beforeSend: function () {
        $('#stepbox9-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-10').show();
          $('.stepbox-9').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-10')))
            .addClass('active');
          $('#stepbox9-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox10() {
  var form = $('.stepbox-10');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail10',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox10-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-11').show();
          $('.stepbox-10').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-11')))
            .addClass('active');
          $('#stepbox10-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  }
  form.addClass('was-validated');
}

function stepBox11() {
  var form = $('.stepbox-11');
  var validator = form.data('validator');
  var fields = form.find('.validate');

  if (fields.valid()) {
    var formSerialize = form.serialize();
    $.ajax({
      url: '/register-detail11',
      method: 'post',
      dataType: 'json',
      data: formSerialize,
      beforeSend: function () {
        $('#stepbox10-btn').prop('disabled', true);
      },
      success: function (response) {
        if (response.success == 1) {
          $('.stepbox-12').show();
          $('.stepbox-11').hide();
          $('#progressbar li')
            .eq($('form').index($('.stepbox-11')))
            .addClass('active');
          $('#stepbox11-btn').prop('disabled', false);
          $('.container, html').animate(
            { scrollTop: $('.container').offset().top },
            'slow'
          );
        }
      },
      error: function (response, xhr, error) {
        location.reload(true);
      },
    });
  } else {
    if ($('#accountTerms').hasClass('error')) {
      $('label[for="accountTerms"]').addClass('error-new');
    } else {
      $('label[for="accountTerms"]').removeClass('error-new');
    }

    if ($('#riskDis').hasClass('error')) {
      $('label[for="riskDis"]').addClass('error-new');
    } else {
      $('label[for="riskDis"]').removeClass('error-new');
    }

    if ($('#pennyStocks').hasClass('error')) {
      $('label[for="pennyStocks"]').addClass('error-new');
    } else {
      $('label[for="pennyStocks"]').removeClass('error-new');
    }

    if ($('#electronicAccess').hasClass('error')) {
      $('label[for="electronicAccess"]').addClass('error-new');
    } else {
      $('label[for="electronicAccess"]').removeClass('error-new');
    }

    if ($('#marginDisclosure').hasClass('error')) {
      $('label[for="marginDisclosure"]').addClass('error-new');
    } else {
      $('label[for="marginDisclosure"]').removeClass('error-new');
    }

    if ($('#w9_Certification').hasClass('error')) {
      $('label[for="w9_Certification"]').addClass('error-new');
    } else {
      $('label[for="w9_Certification"]').removeClass('error-new');
    }

    if ($('#stock_Locate').hasClass('error')) {
      $('label[for="stock_Locate"]').addClass('error-new');
    } else {
      $('label[for="stock_Locate"]').removeClass('error-new');
    }

    if ($('#marginDisc').hasClass('error')) {
      $('label[for="marginDisc"]').addClass('error-new');
    } else {
      $('label[for="marginDisc"]').removeClass('error-new');
    }

    if ($('#confirmedElectronic').hasClass('error')) {
      $('label[for="confirmedElectronic"]').addClass('error-new');
    } else {
      $('label[for="confirmedElectronic"]').removeClass('error-new');
    }
  }
  form.addClass('was-validated');
}

$('.previous').click(function () {
  $(this).parent().hide().prev().show(); //hide parent and show previous
  $('#progressbar li')
    .eq($('form').index($(this)))
    .removeClass('active');
});

$(window).resize(function () {
  if ($(window).width() <= 575) {
    $('.dblogo,.dbnav,.dbadmin').wrapAll(
      '<div id="mobileNav" class="sidenav"></div>'
    );
    $('.sidenav').append(
      "<a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>"
    );
  } else {
    $('.dblogo,.dbnav,.dbadmin').unwrap(
      '<div id="mobileNav" class="sidenav"></div>'
    );
  }
});

function openNav() {
  document.getElementById('mobileNav').style.width = '250px';
}

function closeNav() {
  document.getElementById('mobileNav').style.width = '0';
}

$('.label-icon').click(function () {
  $(this).addClass('radiocurrent').siblings().removeClass('radiocurrent');
});

if (/Mobi/.test(navigator.userAgent)) {
  // if mobile device, use native pickers
  $('.date input').attr('type', 'date');
  $('.time input').attr('type', 'time');
} else {
  // if desktop device, use DateTimePicker
}
