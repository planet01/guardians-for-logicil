const User = require('../models/user/user');
const Country = require('../models/common/countries');
const qs = require('qs');
const configs = require('../../../config/index.js').config;
const encode = require('nodejs-base64-encode');
const axios = require('axios');
const { getCache } = require('../../../lib/utilities/cache');
const moment = require('moment');
const { encryptJsonData, encryptFormData } = require("../../../lib/utilities/encryption.js");
const statesCodes = require("../../../config/statesCodes.json");
const countriesISOCodes = require("../../../config/countriesISOCodes.json");
const { config } = require('dotenv');


const loginRequest = (clientId, clientSecret) => {
    try {
        var data = qs.stringify({
            'grant_type': 'client_credentials'
        });
        var config = {
            method: 'post',
            url: `${configs.accountOpeningAPIBaseURL}/api/connect/token`,
            headers: {
                'Authorization': 'Basic ' + encode.encode(`${clientId}:${clientSecret}`, 'base64'),
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data
        };

        return axios(config);
    }
    catch (e) {
        printErrorMessages('loginRequest', error);
    }
}

const setupUser = async (body) => {
    try {
        var signupInitial = await mapSetupInitialData(body);
        var signupAccountFunding = mapAccountFundingData(body);
        var body = { ...signupInitial, ...signupAccountFunding };
        console.log(body);
        const data = encryptFormData(body);
        var config = {
            method: 'post',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Signup/SignupMergedInitialAndAccount`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                ...data.getHeaders()
            },
            maxContentLength: Infinity,
            maxBodyLength: Infinity,
            data: data
        };
        return axios(config);
    }
    catch (error) {
        printErrorMessages('setupUser', error);
    }
}

const initial = async (body) => {
    try {
        body = await mapSetupInitialData(body);
        console.log(body);
        const data = encryptFormData(body);
        var config = {
            method: 'post',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Signup/Initial`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                ...data.getHeaders()
            },
            maxContentLength: Infinity,
            maxBodyLength: Infinity,
            data: data
        };
        return axios(config);
    }
    catch(e){
        console.log("ERROR " + e);
    }
}

const accountFunding = (body) => {
    var config = {
        method: 'post',
        url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Signup/AccountFunding`,
        headers: {
            'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(encryptJsonData(JSON.parse(body)))
    };

    return axios(config);
}

const otherData = (body, userIdentifierCode) => {
        body = mapOtherData(body);
        console.log(body);
        body.userIdentifierCode = userIdentifierCode;
        var data = encryptFormData(body);
        console.log(body);
        var config = {
            method: 'post',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Signup/OtherData`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                ...data.getHeaders()
            },
            maxContentLength: Infinity,
            maxBodyLength: Infinity,
            data: data
        };

    return axios(config);
}

const openIClearAccount = async (body, accountType) => {
    try {
        var userIdentifierCode = body.userIdentificationCode;
        body = await mapAccountMasterData(body, accountType);
        var config = {
            method: 'post',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/AccountMaster/ProcessAccountMasterData`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                'Content-Type': 'application/json'
            },
            params: { userIdentifierCode: userIdentifierCode },
            data: JSON.stringify(body)
        };
        return axios(config);
    }
    catch (e) {
        printErrorMessages('openIClearAccount', e);
    }
}

const getBridgerResponse = (params) => {
    try {
        var config = {
            method: 'get',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Admin/GetLastUserBridgerResponse`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                'Content-Type': 'application/json'
            },
            params: params
        };
        console.log(config);
        return axios(config);
    }
    catch(e){
        printErrorMessages('getBridgerResponse', e);
    }
}

const getTrueIdResponse = (params) => {
    var config = {
        method: 'get',
        url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Admin/GetLastUserTrueIdResponse`,
        headers: {
            'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
            'Content-Type': 'application/json'
        },
        params: params
    };

    return axios(config);
}

const rerunBridger = async (params) => {
    try {
        console.log("RerunBridger Request");
        var config = {
            method: 'get',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Admin/RerunAndGetUserBridgerResponse`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                'Content-Type': 'application/json'
            },
            params: params
        };
        return axios(config);
    }
    catch(e){
        printErrorMessages('rerunBridger', e);
    }
}

const rerunTrueID = async (params) => {
    try {
        console.log("rerunTrueID Request");
        var config = {
            method: 'get',
            url: `${configs.accountOpeningAPIBaseURL}/api/${configs.accountOpeningAPIVersion}/Admin/RerunAndGetUserTrueIdResponse`,
            headers: {
                'Authorization': `Bearer ${getCache("accountOpeningAccessToken")}`,
                'Content-Type': 'application/json'
            },
            params: params
        };
        return axios(config);
    }
    catch(e){
        printErrorMessages('rerunTrueID', e);
    }
}

const mapSetupInitialData = async (input) => {
    var data = {};
    data["firstName"] = input["firstName"];
    data["lastName"] = input["lastName"];
    data["address"] = input["address"];
    data["dateOfBirth"] = moment(input["dateOfBirth"]).format("DD MMM yyyy");
    data["aptOrSuite"] = input["aptSuite"];
    data["country"] = await mapCountryISO(input["country"]); // '1';//input["country"] == 'US' ? '1' : '2';
    data["state"] = await mapState(input["province"]); //|| 'AL' //'AL'; //;
    data["city"] = input["city"];
    data["zipCode"] = input["zipCode"];
    data["phoneNumber"] = input["teleNum"].substr(input["teleNum"].length - 10);
    data["numberOfDependents"] = input["numberOfDependent"];
    data['maritalStatus'] = input['martialStatus'] != null ? input['martialStatus'].toString()[0] : '-';
    if(input["employee"] == 'retired'){
        data["employmentStatus"] = 'R'
    }
    else if(input["employee"] == 'employed'){
        data["employmentStatus"] = 'E'
    }
    else if(input["employee"] == 'self_employee'){
        data["employmentStatus"] = 'S'
    }
    else if(input["employee"] == 'not_employee'){
        data["employmentStatus"] = 'N'
    }
    else{
        data["employmentStatus"] = '-'
    }
    data["accountFundingAccountType"] = input['accountType'];
    data["accountFundingAccountTitle"] = input['accountName'];
    data["ssn"] = input["taxpayerIDNo"];
    data["validGovtIDType"] = input["idType"].toString()[0];
    data["idNumber"] = input["idNumber"];
    data["issueDate"] = moment(input["issueDate"]).format('DD MMM yyyy');
    data["expirationDate"] = moment(input["expDate"]).format('DD MMM yyyy');
    data["issuingState"] = await mapState(input["placeState"]) || 'AL'//'AL'; //input["placeState"];
    data["issuingCountry"] = await mapCountryISO(input["placeCountry"]) || 'AL'//'AL'; //input["placeState"];
    data["accountTermsAndConditions"] = input["accountTerms"]
    data["dayTradingRiskDisclosure"] = input["riskDis"]
    data["electronicAccessAndTradingAgreement"] = input["electronicAccess"]
    data["stockLocateAgreement"] = input["stock_Locate"]
    data["w9Certification"] = input["w9_Certification"]
    data["taxWithholdingCertification"] = true;
    if(input["significantRisk"] == true){
        data["investmentRiskTolerance"] = "5";
    }
    if(input["moderatelyAggressive"] == true){
        data["investmentRiskTolerance"] = "4";
    }
    if(input["moderate"] == true){
        data["investmentRiskTolerance"] = "3";
    }
    if(input["moderatelyConservative"] == true){
        data["investmentRiskTolerance"] = "2";
    }
    if(input["conservative"] == true){
        data["investmentRiskTolerance"] = "1";
    }
    
    data["isBusinessEntity"] = true;

    if (input.registrationType == 'Individual Account' || input.registrationType == 'JTWROS' || input.registrationType == 'Tenants in Common') {
        data["isBusinessEntity"] = false;
    }

    data["frontIdentification"] = "src\\modules\\public" + input.photo[0].replace(process.env.MEDIAURL, '').replace(/\//g, '\\');
    data["backIdentification"] = "src\\modules\\public" + input.photo[1].replace(process.env.MEDIAURL, '').replace(/\//g, '\\');
    if(data["userSignature"]){
        data["userSignature"] = "src\\modules\\public" + input["signatureName"].replace(process.env.MEDIAURL, '').replace(/\//g, '\\');
    }
    else{
        data["userSignature"] = '';
    }
    var sampleData = {
        "firstName": "Dutton",
        "lastName": "James",
        "address": "999 Block D",
        "dateOfBirth": "02-03-2020",
        "aptOrSuite": "4",
        "country": "1",
        "state": "AL",
        "city": "USA",
        "zipCode": "78582 X2",
        "phoneNumber": "0626555013",
        "numberOfDependents": "0",
        "maritalStatus": "M",
        "employmentStatus": "E",
        "ssn": "123456",
        "validGovtIDType": "D",
        "idNumber": "1234567890",
        "issueDate": "03-02-2021",
        "expirationDate": "03-02-2021",
        "issuingState": "AR",
        "accountTermsAndConditions": "true",
        "dayTradingRiskDisclosure": "false",
        "electronicAccessAndTradingAgreement": "false",
        "stockLocateAgreement": "false",
        "w9Certification": "false",
        "taxWithholdingCertification": "true",
        "investmentRiskTolerance": "5",
     "frontIdentification": "src\\modules\\public\\users\\images\\photo[]_1619626572002_led_purge_mask_4k_5k-HD.jpg",
      "backIdentification": "src\\modules\\public\\users\\images\\photo[]_1619626572043_led_purge_mask_4k_5k-HD.jpg",
      "userSignature": "src\\modules\\public\\users\\images\\photo[]_1619626572043_led_purge_mask_4k_5k-HD.jpg"
    };

    return data;
}

const mapState = async (state) => {
    var stateObj = await Country.findOne({ "states.name": state }).lean();
    stateObj = stateObj.states.find(x => x.name == state);
    return stateObj.state_code;
}

const mapCountryISO = async (country) => {
    var country = await Country.findOne({name: country }).lean();
    return country.iso2;
}

const getTwoDigitIDType = (idType) => {
    var temp = idType.split(' ');
    if (temp.length > 1) {
        return temp[0][0] + temp[1][0];
    }
    return idType[0] + idType[0];
}

const mapAccountMasterData = async (input, accountType) => {
    input = removeComaFromField(input);
    var res = {}
    var entityMapping = configs.entityMapping;
    console.log(entityMapping, input.entity);
    res["addChangeIndicator"] = "A";
    res["correspondent"] = entityMapping[input.entity].corr;
    res["office"] = entityMapping[input.entity].office;
    res["accountNumber"] = input.accountNumber;
    res["accountType"] = accountType;
    if(configs.entity == 'Support' && input.backofficeAccountNumber != null) {
        if(input.backofficeAccountNumber.trim() != ''){
            res["backOfficeAccountNumbers"] = [ input.backofficeAccountNumber ];
        }
    }
    res["subAccountNumber"] = "";
    res["name"] = input.firstName + ' ' + input.lastName; // configs.corr + "-" + configs.office + "-" + input.accountNumber; //corr-office-accNo-accountType;
    res["designator"] = "C";
    res["email"] = input.email;
    res["costDesignator"] = "L";
    
    if (input.registrationType == 'Individual Account') {
        res["legalEntity"] = "INDIV";
    }
    else if(input.registrationType == 'Tenants in Common') {
        res["legalEntity"] = "JTIC";
    } else {
        res["legalEntity"] = "INDIV"
    }
    
    res["rrCode"] = ""; //"123456"; //SampleData
    res["ssn"] = input.taxpayerIDNo;
    
    if (input.w9_Certification) {
        res["w8w9"] = "9"; 
    }
    else{
        res["w8w9"] = "8";
    }

    res["groupCode"] = "";//configs.corr + "-" + configs.office;
    res["shortName"] = ""; //configs.corr + "-" + configs.office + input.accountNumber + "-" + accountType; //corr-office-accNo-accountType;
    res["sweepSymbol"] = "VMMXX";
    res["clientFirstName"] = input.firstName;
    res["clientMiddleInitial"] = ''; //GetInitial(input.initial);
    res["clientLastName"] = input.lastName;
    res["citizenship"] = await mapCountryISO(input.country);

    if (input.w9_Certification) {
        res["taxStatus"] = input.w9_certification_name || ''; 
    }
    else{
        res["taxStatus"] = "W8";
    }

    res["statementsMail"] = "E";
    res["confirmsMail"] = "E";
    res["mailingAddress1"] = input.address;
    res["mailingAddress2"] = input.aptSuite || '';
    res["mailingAddress3"] = "";
    res["city"] = input.city;
    res["state"] = await mapState(input.province) || 'AL';
    res["zip"] = input.zipCode;
    res["clientCountryOfResidency"] = await mapCountryISO(input.country);
    res["taxCountry"] = await mapCountryISO(input.taxResidance);
    res["clientPhone"] = input.teleNum;
    res["dateOfBirth"] = moment(input["dateOfBirth"]).format("MM/DD/yyyy");
    res["jointFirstName"] = "";
    res["jointLastName"] = "";
    res["jointRelation"] = "";
    res["jointSsn"] = "";
    //res["jointDob"] = ""; //Sample will be replaced.
    res["jointCountryOfCitizenship"] = "";
    res["jointW8"] = "N";
    res["idType"] = getTwoDigitIDType(input.idType);
    res["idNumber"] = input.idNumber;
    res["idIssue"] = await mapCountryISO(input.placeCountry);
    res["alertAccessCode"] = "";
    res["dtcParticpant"] = "0294";
    res["institutionalId"] = "";
    res["agentBank"] = "";
    res["accountNoAtAgentBank"] = "";
    res["ipNum1"] = "";
    res["ipAccount1"] = "";
    res["ipNum2"] = "";
    res["ipAccount2"] = "";
    res["itid1"] = "";
    res["itid2"] = "";
    res["itid3"] = "";
    res["mltid"] = "";
    res["statementsCopies"] = "1";
    res["confirmsCopies"] = "1";
    res["interestedPartyName1"] = "";
    res["interestedPartyEmail1"] = "";
    res["interestedPartyName2"] = "";
    res["interestedPartyEmail2"] = "";
    res["ssnType"] = "SSN";
    res["margin"] = "Y";
    res["dayTrader"] = (input.accountRisk == "Active or Day Trading" ? "Y" : "N");
    res["timeTickOrDTCall"] = "Y";
    return res;
}

const mapAccountFundingData = (input) => {
    var incomeSource = [];
    var otherIncome = "";
    if (input.incomeAccount) {
        incomeSource.push("1");
    }
    if (input.retireSaving) {
        incomeSource.push("2");
    }
    if (input.gift) {
        incomeSource.push("3");
    }
    if (input.businessProp) {
        incomeSource.push("4");
    }
    if (input.inheritence) {
        incomeSource.push("5");
    }
    if (input.secuBnefit) {
        incomeSource.push("6");
    }
    if (input.otherCheckbox) {
        incomeSource.push("7");
        otherIncome = input.otherInput;
    }
    var data = {
        userIdentifierCode: "",
        incomeSource: JSON.stringify(incomeSource),
        otherIncomeSource: otherIncome,
        accountFundingBankName: input.bankFunding,
        abaOrSWIFT: input.abaSwift,
        accountFundingBankAccountNumber: input.accountNumber
    }
    return data;
}

const mapOtherData = (input) => {
    
    var annualExpenses = ['$50,000 & Under', '$50,001 - $100,000', '$100,001 - $250,000', '$250,001 - $500,000', 'Over $500,000'];
    var specialExpenses = ['$50,000 and under', '$50,001 - $100,000', '$100,001 ‐250,000', '$250,001 ‐500,000', 'Over $500,000'];
    var annualIncome = ['$25,000 and under', '$25,001 - $50,000', '$50,001 - $100,000', '$100,001 - $250000', '$250,001 ‐ $500,000', 'over $500,000'];
    var netWorth = ['$25,000 and Under', '$25,001 - $50,000', '$50,001 - $200,000', '$200,001 - $500,000', '$500,001 - $1,000,000', 'over $1,000,000'];
    var liquidNetWorth = ['$25,000 and Under', '$25,001 - $50,000', '$50,001 - $200,000', '$200,001 - $500,000', '$500,001 - $1,000,000', 'over $1,000,000'];
    var taxRate = ['0-5', '16-25', '26-30', '31-35', 'over 35'];
    var accountRisk = ['Active or Day Trading', 'Short Term Trading'];
    // var annualExpenses = ['$25,000 and under', '$50,001 - $100,000', '$100,001 - $250,000', '$250,001 - $500,000', 'Over $500,000'];
    // var specialExpenses = ['$25,000 and under', '$50,001 - $100,000', '$100,001 - $250,000', '$250,001 - $500,000', 'Over $500,000'];
    var liquidityNeeds = ['Very important', 'Important', 'Some what important', 'Does not matter']
    var timeToAchieveFinancialGoal = ['Under 1 year', '1 - 2', '3 - 5', '6 - 10', '11 - 20', 'Over 20'];
    var assetClass = ['Stock', 'Bond', 'Options', 'Future'];
    var yearsOfExperience = ['0', '1-5', '5+'];
    var knowledge = ['None', 'Limited', 'Good', 'Extensive'];
    var assetClasses = [];
    if(input.investStock){
        assetClasses.push({assetClass: assetClass.indexOf("Stock") + 1, yearsOfExperience: yearsOfExperience.indexOf(input.stockExp) + 1, knowledge: knowledge.indexOf(input.stockExpertise)});
    }
    if(input.investBond){
        assetClasses.push({assetClass: assetClass.indexOf("Bond") + 1, yearsOfExperience: yearsOfExperience.indexOf(input.bondExp) + 1, knowledge: knowledge.indexOf(input.bondExpertise)});
    }
    if(input.investOptions){
        assetClasses.push({assetClass: assetClass.indexOf("Options") + 1, yearsOfExperience: yearsOfExperience.indexOf(input.optionsExp) + 1, knowledge: knowledge.indexOf(input.optionsExpertise)});
    }
    if(input.investFuture){
        assetClasses.push({assetClass: assetClass.indexOf("Future") + 1, yearsOfExperience: yearsOfExperience.indexOf(input.futureExp) + 1, knowledge: knowledge.indexOf(input.futureExpertise)});
    }

    var data = {
        annualIncome: annualIncome.indexOf(input.annualIncome) + 1,
        netWorthExcludingResidence: netWorth.indexOf(input.netWorth) + 1,
        liquidNetWorth: liquidNetWorth.indexOf(input.liquidNetWorth) + 1,
        taxRate: taxRate.indexOf(input.taxRate) + 1,
        investmentObjectives: accountRisk.indexOf(input.accountRisk) + 1,
        annualExpenses: annualExpenses.indexOf(input.annualExpense) + 1,
        specialExpenses: specialExpenses.indexOf(input.specialExpense) + 1,
        liquidityNeeds: liquidityNeeds.indexOf(input.liquidityExpense) + 1,
        timeToAchieveFinancialGoal: timeToAchieveFinancialGoal.indexOf(input.financialGoal) + 1,
        assetClasses: assetClasses,
        haveOtherBrokerageAccounts: input.brokerAccount == "1",
        numberOfBrokerageAccounts: input.brokerAccount == "1" ? input.brokerAccountTotal : 0,
        
        haveOtherVelocityAccounts: input.beneficial,
        haveOtherVelocityAccountsNumber: input.beneficial ? input.beneficialAccountNumber : '',
        haveOtherVelocityAccountsName: input.beneficial ? input.beneficialAccountName : '',

        knowAnyVelocityAccountHolder: input.shareHolder,
        knowAnyVelocityAccountHolderNumber: input.shareHolder ? input.shareHolderAccountNumber : '',
        knowAnyVelocityAccountHolderName: input.shareHolder ? input.shareHolderAccountName : '',
        knowAnyVelocityAccountHolderRelationship: input.shareHolder ? input.shareHolderRelationship : '',

        shareholderInPubliclyTradedCompany: input.immediate,
        shareholderInPubliclyTradedCompanyName: input.immediate ? input.immediateCompanyName : '',
        shareholderInPubliclyTradedCompanyAddress: input.immediate ? input.immediateCompanyaddress : '',
        shareholderInPubliclyTradedCompanyRelationship: input.immediate ? input.immediateRelationshipwithentity : '',

        registeredBrokerDealerSecuritiesExchangeOrFINRA: input.securities,
        registeredBrokerDealerSecuritiesExchangeOrFINRAName :input.securities ? input.securitiesFirm : '',
        registeredBrokerDealerSecuritiesExchangeOrFINRAAddress :input.securities ? input.securitiesFirmAddress : '',
        registeredBrokerDealerSecuritiesExchangeOrFINRAPermissionToAccountOpeningFile :input.securities ? "src\\modules\\public\\" + input.securitiesPermissionAccount.replace(/\//g, '\\') : '',

        seniorOfficerAtFinancialInstitution: input.institution,
        seniorOfficerAtFinancialInstitutionName: input.institution ? input.institutionFirm : '',
        seniorOfficerAtFinancialInstitutionAddress: input.institution ? input.institutionFirmAddress : '',
        seniorOfficerAtFinancialInstitutionPosition: input.institution ? input.institutionPositioninFirm : '',

        knowAnyPublicOrPoliticalFigure: input.authorizedIndividual
    }
    return data;
}

const GetInitial = (data) => {
    var init = 'Z';
    if(data){
        var a = data.split(' ');
        if(a.length > 0){
            return a[0];
        }
    }
    return init;
}

const printErrorMessages = (key, error) => {
    console.log(key, error);
    if (error.response.status == 401)
        console.log(key, "Please login first.");
    else if (error.response.status == 400) {
        console.log(key, error.response.data);
    }
    else {
        console.log(key, error);
    }
}

const removeComaFromField = (input) => {
    var output = {};
    var byPassKeys = ['photo', 'dateOfBirth', 'expDate', 'issueDate', 'createdAt', 'updatedAt']
    Object.keys(input).forEach(function(key, index) {
        console.log("KEY", key);
        console.log("INPUT[KEY]", input[key]);
        if(input[key] != null && input[key] != undefined && !byPassKeys.includes(key)) {
            output[key] = input[key].toString().replace(/,/g, '');
            console.log("OUTPUT[KEY]", output[key]);
        }
        else{
            output[key] = input[key];
        }
    });
    return output;
}
module.exports = { loginRequest, initial, accountFunding, otherData, getBridgerResponse, getTrueIdResponse, openIClearAccount, setupUser, rerunBridger, rerunTrueID };
