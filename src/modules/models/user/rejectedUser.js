const mongoose = require('mongoose');
var rejectedUserSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    firstName: {
        type: String,
        default: null
    },
    lastName: {
        type: String,
        default: null
    },
    reason: {
        type: String,
        default: null
    },
    applicationDetail: {
        type: JSON,
        default: null
    },
}, {
    timestamps: true
});


var rejectedUser = mongoose.model('rejectedUser', rejectedUserSchema);

module.exports = rejectedUser;