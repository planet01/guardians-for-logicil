const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

/**
 * User Schema
 */
const configurationSchema = new mongoose.Schema(
  {
    service: {
      type: String,
      required: true,
    },
    host: {
      type: String,
      required: true,
    },
    port: {
      type: Number,
      required: true,
    },
    hostemail: {
      type: String,
      required: true,
    },
    hostpassword: {
      type: String,
      required: true,
    },

    rejectedtemp: {
      subject: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },
    },

    acceptedtemp: {
      subject: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Configuration', configurationSchema);
