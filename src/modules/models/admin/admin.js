const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const SALT_FACTOR = 10;

const adminSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
      unique: true,
    },
    role: {
      type: String,
      enum: ['Admin', 'Marketing'],
      default: 'Admin',
    },
    isForgot:
    {
      type: Boolean,
      default: null
    },
    forgotToken:
    {
      type: String,
      default: null
    },
    signatureName: { type: String, default: null},
  },
  { timestamps: true }
);

adminSchema.pre('save', function (done) {
  var admin = this;

  if (!admin.isModified('password')) {
    return done();
  }

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) {
      return done(err);
    }
    bcrypt.hash(admin.password, salt, function (err, hashedPassword) {
      if (err) {
        return done(err);
      }
      admin.password = hashedPassword;
      done();
    });
  });
});

adminSchema.pre('findOneAndUpdate', function (done) {
  var admin = this;
  if (!admin._update.password) {
    return done();
  }

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) {
      return done(err);
    }
    bcrypt.hash(admin._update.password, salt, function (err, hashedPassword) {
      if (err) {
        return done(err);
      }
      admin._update.password = hashedPassword;
      done();
    });
  });
});

module.exports = mongoose.model('Admin', adminSchema);
