const User = require('../models/user/user');
const { postSendEmail } = require('../config/mail');
const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
  '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'
);
const nodemailer = require('nodemailer');
const Configuration = require('../models/admin/configuration');
let transport;
let hostEmail;
const configuration = Configuration.findOne(
  {},
  'service host port hostemail hostpassword'
)
  .then((config) => {
    if (config) {
      hostEmail = config.hostemail;
      transport = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        auth: {
            user: config.hostemail,
            pass: config.hostpassword
        }
      });
      } else {
        hostEmail = 'logiciel.subscriptions@logicielservice.com';
      transport = nodemailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        auth: {
            user: 'logiciel.subscriptions@logicielservice.com',
            pass: 'LogiEmail!2345'
        }
      });
    }
  })
  .catch((err) => console.log(err));


exports.getEmailVerify = (req, res) => {
  if (req.session.application) {
    return res.render('user/email-verify');
  } else {
    return res.redirect('/login');
  }
};

exports.postEmailVerify = async (req, res) => {
  try {
    const verification_code = req.body.verification_code;

    const user = await User.findOne({
      _id: req.session.application._id,
      'emailVerify.verification_code': verification_code,
    });

    if (!user) {
      req.flash('error', 'Verification Code Is Incorrect');
      return res.redirect('/email-verify');
    } else {
      const updateUser = await User.findByIdAndUpdate(
        user._id,
        {
          $set: {
            'emailVerify.status': true,
            'emailVerify.verification_code': null,
          },
        },
        { new: true }
      );
      req.session.application = updateUser;
      return res.redirect('/register-detail?id=' + updateUser._id);
    }
  } catch (err) {
    console.log(err);
  }
};

exports.ResendEmail = async (req, res) => {
  try {
    const verification_code = Math.random().toString(36).substring(7);
    // const text =
    //   '<h4><b>Email Verification</b></h4>' +
    //   '<p> Your Email Verification Code is : ' +
    //   verification_code +
    //   '</p>' +
    //   '<br><br>' +
    //   '<p>Guardian Trading LLC</p>';
      
    // sgMail
    //   .send({
    //     to: req.session.application.email,
    //     from: process.env.SENDGRIDEMAIL,
    //     subject: 'Email Verification',
    //     html: text,
    //   })
    //   .then(() => {
    //     console.log('Email sent');
    //   })
    //   .catch((error) => {
    //     console.error(error.response.body);
    //   });
      const message = {
				from: hostEmail,
				to: req.session.application.email,
				subject: 'Email Verification',
				html: '<h4><b>Email Verification</b></h4>' +
        '<p> Your Email Verification Code is : ' +
        verification_code +
        '</p>' +
        '<br><br>' +
        '<p>Guardian Trading LLC</p>'
			};
      console.log("transport", transport);
			transport.sendMail(message, function(err, info) {
				if (err) {
				  console.log(err)
				} else {
				  console.log(info);
				}
			});

    //   postSendEmail(req.session.application.email, 'Email Verification', text);

    const user = await User.findByIdAndUpdate(
      req.session.application._id,
      {
        'emailVerify.status': false,
        'emailVerify.verification_code': verification_code,
      },
      { new: true }
    );
    req.session.application = user;

    return res.redirect('/email-verify');
  } catch (err) {
    console.log(err);
  }
};
