const User = require('../models/user/user');
const { postSendEmail } = require('../config/mail');
const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
  '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'
);
const nodemailer = require('nodemailer');
const Configuration = require('../models/admin/configuration');
let transport;
let hostEmail;
const configuration = Configuration.findOne(
  {},
  'service host port hostemail hostpassword'
)
  .then((config) => {
    if (config) {
      hostEmail = config.hostemail;
      transport = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        auth: {
            user: config.hostemail,
            pass: config.hostpassword
        }
      });
      } else {
        hostEmail = 'logiciel.subscriptions@logicielservice.com';
      transport = nodemailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        auth: {
            user: 'logiciel.subscriptions@logicielservice.com',
            pass: 'LogiEmail!2345'
        }
      });
    }
  })
  .catch((err) => console.log(err));

exports.getForgotPassword = (req, res) => {
  res.render('user/forgot-password');
};

exports.postForgotPassword = async (req, res) => {
  try {
    const email = req.body.email;

    const user = await User.findOne({ email: email });

    if (!user) {
      req.flash('error', 'User doesn’t exist');
      return res.redirect('/forgot-password');
    } else {
      const token = crypto.randomBytes(32).toString('hex');
      const subject = 'Reset your account password';
      const text =
        '<h4><b>Reset Password</b></h4>' +
        '<p>To reset your password click the link below</p>' +
        '<a href=' +
        req.protocol +
        '://' +
        req.get('host') +
        '/new-password/' +
        user.id +
        '/' +
        token +
        '>' +
        req.protocol +
        '://' +
        req.get('host') +
        '/new-password/' +
        user.id +
        '/' +
        token +
        '</a>' +
        '<br><br>' +
        '<p>Guardian Trading LLC</p>';

      // sgMail
      //   .send({
      //     to: email,
      //     from: process.env.SENDGRIDEMAIL,
      //     subject: subject,
      //     html: text,
      //   })
      //   .then(() => {
      //     console.log('Email sent');
      //   })
      //   .catch((error) => {
      //     console.error(error.response.body);
      //   });
        const message = {
					from: hostEmail,
					to: email,
					subject: subject,
					html: text
				};
				transport.sendMail(message, function(err, info) {
					if (err) {
					  console.log(err)
					} else {
					  console.log(info);
					}
				});
      // postSendEmail(email, subject, text);

      const updateUser = await User.findOneAndUpdate(
        { email: email },
        { $set: { isForgot: true, forgotToken: token } }
      );

      return res.redirect('/login');
    }
  } catch (err) {
    console.log(err);
  }
};

exports.getError = (req, res) => {
  res.render('user/error');
};

exports.getNewPassword = async (req, res) => {
  const user = await User.findOne({
    _id: req.params.id,
    forgotToken: req.params.token,
  }).lean();

  if (user) {
    return res.render('user/new-password', { id: req.params.id });
  }
  return res.redirect('/error');
};

exports.postNewPassword = async (req, res) => {
  const updateUser = await User.findByIdAndUpdate(req.params.id, {
    password: req.body.password,
    isForgot: false,
    forgotToken: null,
  });

  res.redirect('/login');
};
