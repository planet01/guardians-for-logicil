const appConfig = require('../../../config/index').config;
const User = require('../models/user/user');

exports.getUserList = async(req, res) => {
	try{
		if(appConfig.entity == "Support"){
			var users = [];
			await User.find({ isCreatedBySupportUser: true }, function (err, user) {
				for(var i = 0; i < user.length; i++){
					users.push({ firstName: user[i].firstName, lastName: user[i].lastName, email: user[i].email, state: user[i].province, country: user[i].country, isDraft: user[i].isDraft, isApprove: user[i].isApprove, id: user[i]._id })
				}
			});
	
			res.render('user/user-list', { users: users });
		}
		else{
			if (appConfig.entity != "Support") {
				return res.redirect('register-detail');
			}
		}
	}
	catch(err){
		console.log(err);
	}
}

exports.redirectRegister = async(req, res) => {
    var user = await User.findOne({ _id: req.params.id }).lean();
	if (user) {
        if(user.isApprove) {
	    	return res.redirect('accepted-form');
        }
    }

    res.redirect('/register-detail?id=' + (req.params.id == undefined ? '' : req.params.id));
}