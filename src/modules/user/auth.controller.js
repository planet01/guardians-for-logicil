const bcrypt = require('bcryptjs');
const User = require('../models/user/user');
const Audit = require('../models/common/audit');
const AccountOpenignAPIController = require('../admin/account.opening.api.controller')
const dd = require('dump-die')
const fs = require('fs');
const pdfshift = require('pdfshift')('66ce23aef8e44ee29131d342db95a021');
const nodemailer = require('nodemailer');
const Configuration = require('../models/admin/configuration');
const appConfig		= require('../../../config/index.js').config;
let transport;
let hostEmail;
const configuration = Configuration.findOne(
  {},
  'service host port hostemail hostpassword'
)
  .then((config) => {
    if (config) {
      hostEmail = config.hostemail;
      transport = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        auth: {
            user: config.hostemail,
            pass: config.hostpassword
        }
      });
      } else {
        hostEmail = 'logiciel.subscriptions@logicielservice.com';
      transport = nodemailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        auth: {
            user: 'logiciel.subscriptions@logicielservice.com',
            pass: 'LogiEmail!2345'
        }
      });
    }
  })
  .catch((err) => console.log(err));
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
	'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'
);

const {
	postSendEmail
} = require('../config/mail');

exports.getLogin = (req, res) => {
	res.render('user/login');
};
exports.postLogin = async (req, res, next) => {
	try {
		const user = await User.findOne({
			email: { '$regex': req.body.email, '$options': 'i' }
		});

		if (!user) {
			req.flash('error', 'That Email is Not Registered');
			return res.redirect('/login');
		}
		if(appConfig.entity != user.entity){
			req.flash('error', 'User not allowed');
			return res.redirect('/login');
		}

		if (user) {
			const passwordCompare = await bcrypt.compare(
				req.body.password,
				user.password
			);

			if (!passwordCompare) {
				req.flash('error', 'Password Incorrect');
				return res.redirect('/login');
			}
			req.session.application = user;
			if (user.entity != "Support" && user.isApprove == true) {
				return res.redirect('accepted-form');
			}
			if(user.entity != "Support"){
				res.redirect('/register-detail?id=' + (user.id == undefined ? '' : user.id));
			}
			else{
				return res.redirect('user-list');
			}
		}
	} catch (err) {
		console.log(err);
	}
};
exports.checkEmail = async (req, res, next) => {
	try {
		const user = await User.findOne({
			email: {'$regex': req.body.email, '$options': 'i' }
		});

		if (user) {
			res.json({
				success: 1
			});
		} else {
			res.json({
				success: 0
			});
		}
	} catch (err) {
		console.log(err);
	}
};
exports.getAcceptedForm = async (req, res) => {
	if (!req.session.application.isApprove) {
		return res.redirect('register-detail');
	}
	res.render('user/accepted-form');
};
exports.getRegister = async (req, res) => {
	res.render('user/signup');
};

exports.postRegister = async (req, res) => {
	try {
		const email = req.body.email;
		const password = req.body.password;
		const registrationType = req.body.registrationType;
		const productTrade = req.body.productTrade;
		const hearAbout = req.body.hearAbout;
		const entity = appConfig.entity;
		const user = await User.findOne({
			email: {'$regex': email, '$options': 'i' }
		});
		const verification_code = Math.random().toString(36).substring(7);
		if (user) {
			req.flash('error', 'User already exists');
			return res.redirect('/register');
		} else {
			let newUser = new User({
				email: email,
				password: password,
				registrationType,
				productTrade,
				hearAbout,
				entity,
				'emailVerify.verification_code': verification_code,
			});
			await newUser.save();
			let userAudit = new Audit({
				userId: newUser._id,
				'signupForm.type': 'Signup',
				'signupForm.location': req.body.lat + ' ' + req.body.long,
				'signupForm.ipaddress': req.headers['x-forwarded-for'] || req.connection.remoteAddress,
				'signupForm.date': Date.now(),
			});
			await userAudit.save();
			req.session.application = newUser;

			// const text =
			// 	'<h4><b>Email Verification</b></h4>' +
			// 	'<p> Your Email Verification Code is : ' +
			// 	verification_code +
			// 	'</p>' +
			// 	'<br><br>' +
			// 	'<p>Guardian Trading LLC</p>';

			
			const message = {
				from: hostEmail,
				to: newUser.email,
				subject: 'Email Verification',
				html: '<h4><b>Email Verification</b></h4>' +
						'<p> Your Email Verification Code is : ' +
						verification_code +
						'</p>' +
						'<br><br>' +
						'<p>Guardian Trading LLC</p>'
			};
			transport.sendMail(message, function(err, info) {
				if (err) {
				  console.log(err)
				} else {
				  console.log(info);
				}
			});
			// sgMail
			// 	.send({
			// 		to: newUser.email,
			// 		from: process.env.SENDGRIDEMAIL,
			// 		subject: 'Email Verification',
			// 		html: text,
			// 	})
			// 	.then(() => {
			// 		console.log('Email sent');
			// 	})
			// 	.catch((error) => {
			// 		console.error(error.response.body);
			// 	});

			// postSendEmail(newUser.email, 'Email Verification', text);

			return res.redirect('/email-verify');
		}
	} catch (err) {
		console.log(err);
	}
};

exports.getRegisterDetail = async (req, res) => {
	var isAlreadyExists = false;
	var user = { isDraft: true };
	if(req.query.id != undefined && req.query.id.trim() != '') {
		user = await User.findOne({ _id: req.query.id }).lean();
		if (user && user.isApprove) {
			return res.redirect('accepted-form');
		}
		isAlreadyExists = true;
	}
	if(appConfig.entity != 'Support'){
		if (user.isApprove) {
			return res.redirect('accepted-form');
		}
	}
	if(user.photo == null) { user.photo = []; }
	res.render('user/register-detail', { user: user, isAlreadyExists: isAlreadyExists });
};

exports.postRegisterDetail0 = async (req, res) => {
	try {
		req.body.email =
			typeof req.body.email !== 'undefined' ?
			req.body.email :
			false;
			
		var user = await User.findOne({ email: {'$regex': req.body.email, '$options': 'i' } }).lean();

		if(user == null){
			// if(user != null){
				// await User.findOneAndUpdate({"email": req.body.email},
				// { email: req.body.email, 'emailVerify.status': true }, { new: true, },
				// function(err, _user){
				// 	user = _user;
				// });

			// }
			// else {
				let newUser = new User({
					email: req.body.email,
					hearAbout: 'Support',
					productTrade: req.body.productTrade,
					registrationType: req.body.registrationType,
					password: req.body.email,
					'emailVerify.status': true,
					entity: req.body.entity,
					isCreatedBySupportUser: true,
				});
				await newUser.save();
				let userAudit = new Audit({
					userId: newUser._id,
					'signupForm.type': 'Signup',
					'signupForm.location': req.body.lat + ' ' + req.body.long,
					'signupForm.ipaddress': req.headers['x-forwarded-for'] || req.connection.remoteAddress,
					'signupForm.date': Date.now(),
				});
				await userAudit.save();
				user = newUser;
			// }
			res.json({
				success: 1,
				user: user
			});
		}
		else{
			res.json({
				success: 0,
				message: 'Email already exist'
			})
		}		
	} 
	catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail1 = async (req, res) => {
	try {
		req.body.mailAddother =
			typeof req.body.mailAddother !== 'undefined' ?
			req.body.mailAddother :
			false;
		req.body.trustedPerson =
			typeof req.body.trustedPerson !== 'undefined' ?
			req.body.trustedPerson :
			false;

		req.body.initial =
			req.body.firstName.charAt(0) + ' ' + req.body.lastName.charAt(0);
		if (!req.body.mailAddother) {
			req.body.mailingAddress = null;
			req.body.mailingAptsuite = null;
			req.body.mailingCountry = null;
			req.body.mailingProvince = null;
			req.body.mailingCity = null;
			req.body.mailingZipcode = null;
		}

		if (!req.body.trustedPerson) {
			req.body.trustedPersonName = null;
			req.body.trustedPersonPhone = null;
			req.body.trustedPersonEmail = null;
			req.body.trustedPersonStreet = null;
			req.body.trustedPersonCountry = null;
			req.body.trustedPersonState = null;
			req.body.trustedPersonCity = null;
			req.body.trustedPersonZip = null;
		}

		if (!req.body.companyMailAddother) {
			req.body.companyMailAddress = null;
			req.body.companyMailAptSuite = null;
			req.body.companyMailCountry = null;
			req.body.companyMailStateProvince = null;
			req.body.companyMailCity = null;
			req.body.companyMailZipCode = null;
		}

		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail2 = async (req, res) => {
	try {
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail3 = async (req, res) => {
	try {
		if (req.body.idType != 'Local ID') {
			req.body.nameOfId = null;
		}
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail4 = async (req, res) => {
	try {
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail5 = async (req, res) => {
	try {
		req.body.incomeAccount =
			typeof req.body.incomeAccount !== 'undefined' ?
			req.body.incomeAccount :
			false;
		req.body.retireSaving =
			typeof req.body.retireSaving !== 'undefined' ?
			req.body.retireSaving :
			false;
		req.body.gift =
			typeof req.body.gift !== 'undefined' ? req.body.gift : false;
		req.body.businessProp =
			typeof req.body.businessProp !== 'undefined' ?
			req.body.businessProp :
			false;
		req.body.inheritence =
			typeof req.body.inheritence !== 'undefined' ?
			req.body.inheritence :
			false;
		req.body.secuBnefit =
			typeof req.body.secuBnefit !== 'undefined' ? req.body.secuBnefit : false;
		req.body.otherCheckbox =
			typeof req.body.otherCheckbox !== 'undefined' ?
			req.body.otherCheckbox :
			false;
		req.body.otherInput =
			req.body.otherCheckbox !== '' ? req.body.otherInput : null;

		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail6 = async (req, res) => {
	try {
		req.body.conservative =
			typeof req.body.conservative !== 'undefined' ?
			req.body.conservative :
			false;
		req.body.moderatelyConservative =
			typeof req.body.moderatelyConservative !== 'undefined' ?
			req.body.moderatelyConservative :
			false;

		req.body.moderate =
			typeof req.body.moderate !== 'undefined' ? req.body.moderate : false;

		req.body.moderatelyAggressive =
			typeof req.body.moderatelyAggressive !== 'undefined' ?
			req.body.moderatelyAggressive :
			false;

		req.body.significantRisk =
			typeof req.body.significantRisk !== 'undefined' ?
			req.body.significantRisk :
			false;

		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail7 = async (req, res) => {
	try {
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail8 = async (req, res) => {
	try {
		// console.log(req.body);
		if (!req.body.investStock) {
			req.body.investStock = null;
			req.body.stockExp = null;
			req.body.stockExpertise = null;
		}
		if (!req.body.investBond) {
			req.body.investBond = null;
			req.body.bondExp = null;
			req.body.bondExpertise = null;
		}
		if (!req.body.investOptions) {
			req.body.investOptions = null;
			req.body.optionsExp = null;
			req.body.optionsExpertise = null;
		}
		if (!req.body.investFuture) {
			req.body.investFuture = null;
			req.body.futureExp = null;
			req.body.futureExpertise = null;
		}
		
		if (req.body.investStock) {
			req.body.stockExp =
				typeof req.body.stockExp !== 'undefined' ? req.body.stockExp : null;
			req.body.stockExpertise =
				typeof req.body.stockExpertise !== 'undefined' ?
				req.body.stockExpertise :
				null;
		}
		if (req.body.investBond) {
			req.body.bondExp =
				typeof req.body.bondExp !== 'undefined' ? req.body.bondExp : null;
			req.body.bondExpertise =
				typeof req.body.bondExpertise !== 'undefined' ?
				req.body.bondExpertise :
				null;
		}
		if (req.body.investOptions) {
			req.body.optionsExp =
				typeof req.body.optionsExp !== 'undefined' ? req.body.optionsExp : null;
			req.body.optionsExpertise =
				typeof req.body.optionsExpertise !== 'undefined' ?
				req.body.optionsExpertise :
				null;
		}
		if (req.body.investFuture) {
			req.body.futureExp =
				typeof req.body.futureExp !== 'undefined' ? req.body.futureExp : null;
			req.body.futureExpertise =
				typeof req.body.futureExpertise !== 'undefined' ?
				req.body.futureExpertise :
				null;
		}
		console.log(req.body);
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});
		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail9 = async (req, res) => {
	try {
		const user = await User.findById(req.body.id).lean();
		if(user.photo==null){
			let photo0 = user.photo[0].split('/');
			let photo1 = user.photo[1].split('/');
			let photo2 = user.photo[2].split('/');
			let photo3 = user.photo[3].split('/');
			if (user) {
				const fileExist0 = fs.existsSync('./src/modules/public/users/images/' + photo0[photo0.length - 1]);
				const fileExist1 = fs.existsSync('./src/modules/public/users/images/' + photo1[photo1.length - 1]);
				const fileExist2 = fs.existsSync('./src/modules/public/users/images/' + photo2[photo2.length - 1]);
				const fileExist3 = fs.existsSync('./src/modules/public/users/images/' + photo3[photo3.length - 1]);
				if (fileExist0 && photo0.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo0[photo0.length - 1]
					);
				}
				if (fileExist1 && photo1.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo1[photo1.length - 1]
					);
				}
				if (fileExist2 && photo2.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo2[photo2.length - 1]
					);
				}
				if (fileExist3 && photo3.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo3[photo3.length - 1]
					);
				}
			}
			req.body.photo = [
				req.files[0] ? `/users/images/${req.files[0].filename}` : 'none',
				req.files[1] ? `/users/images/${req.files[1].filename}` : 'none',
			];
		}else{
			req.body.photo = [
				req.files[0] ? `/users/images/${req.files[0].filename}` : user.photo[0],
				req.files[1] ? `/users/images/${req.files[1].filename}` : user.photo[1],
			];
		}
		if (user.registrationType=="Corporate" || user.registrationType=="Limited Liability Company" || user.registrationType=="Limited Partnership") {
			if(user.photo==null){
				let photo0 		 = user.photo[4]==undefined?"":user.photo[4].split('/');
				let photo1 		 = user.photo[5]==undefined?"":user.photo[5].split('/');
				let photo2 		 = user.photo[6]==undefined?"":user.photo[6].split('/');
				let photo3 		 = user.photo[7]==undefined?"":user.photo[7].split('/');
				const fileExist0 = fs.existsSync('./src/modules/public/users/images/' + photo0[photo0.length - 1]);
				const fileExist1 = fs.existsSync('./src/modules/public/users/images/' + photo1[photo1.length - 1]);
				const fileExist2 = fs.existsSync('./src/modules/public/users/images/' + photo2[photo2.length - 1]);
				const fileExist3 = fs.existsSync('./src/modules/public/users/images/' + photo3[photo3.length - 1]);
				if (fileExist0 && photo0.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo0[photo0.length - 1]
					);
				}
				if (fileExist1 && photo1.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo1[photo1.length - 1]
					);
				}
				if (fileExist2 && photo2.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo2[photo2.length - 1]
					);
				}
				if (fileExist3 && photo3.length > 1) {
					fs.unlinkSync(
						'./src/modules/public/users/images/' + photo3[photo3.length - 1]
					);
				}
				req.body.photo[2] = req.files[2] ? `/users/images/${req.files[2].filename}` : 'none'
				req.body.photo[3] = req.files[3] ? `/users/images/${req.files[3].filename}` : 'none'
				req.body.photo[4] = req.files[4] ? `/users/images/${req.files[4].filename}` : 'none'
				req.body.photo[5] = req.files[5] ? `/users/images/${req.files[5].filename}` : 'none'
			}else{
				req.body.photo[2] = req.files[2] ? `/users/images/${req.files[2].filename}` : user.photo[2]
				req.body.photo[3] = req.files[3] ? `/users/images/${req.files[3].filename}` : user.photo[3]
				req.body.photo[4] = req.files[4] ? `/users/images/${req.files[4].filename}` : user.photo[4]
				req.body.photo[5] = req.files[5] ? `/users/images/${req.files[5].filename}` : user.photo[5]
			}
		}
		req.body = addBaseUrlWithImagePath(req.body);

		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			new: true,
			upsert: true,
		});

		if (updateUser) {
			req.session.application = updateUser;

			res.json({
				success: 1,
				updateUser
			});
		} else {
			res.json({
				success: 0,
				updateUser
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail10 = async (req, res) => {
	try {
		const userCheck = await User.findById(req.body.id).lean();
		if (
			userCheck.securitiesPermissionAccount != null &&
			userCheck.securitiesPermissionAccount != ''
		) {
			const fileExist = fs.existsSync(
				'./src/modules/public/users/images/' +
				userCheck.securitiesPermissionAccount
			);
			if (fileExist) {
				fs.unlinkSync(
					'./src/modules/public/users/images/' +
					userCheck.securitiesPermissionAccount
				);
			}
		}
		
		// req.body.investStock =
		// 	typeof req.body.investStock !== 'undefined' ?
		// 	req.body.investStock :
		// 	false;
		// req.body.investBond =
		// 	typeof req.body.investBond !== 'undefined' ? req.body.investBond : false;
		// req.body.investOptions =
		// 	typeof req.body.investOptions !== 'undefined' ?
		// 	req.body.investOptions :
		// 	false;
		// req.body.investFuture =
		// 	typeof req.body.investFuture !== 'undefined' ?
		// 	req.body.investFuture :
		// 	false;

		req.body.brokerAccount =
			typeof req.body.brokerAccount !== 'undefined' ?
			req.body.brokerAccount :
			null;
		req.body.beneficial =
			typeof req.body.beneficial !== 'undefined' ? req.body.beneficial : null;
		req.body.shareHolder =
			typeof req.body.shareHolder !== 'undefined' ? req.body.shareHolder : null;
		req.body.immediate =
			typeof req.body.immediate !== 'undefined' ? req.body.immediate : null;
		req.body.securities =
			typeof req.body.securities !== 'undefined' ? req.body.securities : null;

		req.body.institution =
			typeof req.body.institution !== 'undefined' ? req.body.institution : null;

		req.body.immediateCompanyName = typeof req.body.immediate ?
			req.body.immediateCompanyName :
			null;
		req.body.securitiesFirm = typeof req.body.securities ?
			req.body.securitiesFirm :
			null;
		req.body.institutionFirm = typeof req.body.institution ?
			req.body.institutionFirm :
			null;
		req.body.securitiesPermissionAccount = req.file ?
			`/users/images/${req.file.filename}` :
			'';

		// if (!req.body.houseHold) {
		//   req.body.compnyName = null;
		//   req.body.symbol = null;
		// }

		// if (!req.body.municipal) {
		//   req.body.affiName = null;
		//   req.body.affiEntityName = null;
		//   req.body.affiAdre = null;
		//   req.body.affiCountry = null;
		//   req.body.affiState = null;
		//   req.body.affiZip = null;
		// }

		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		if (updateUser) {
			req.session.application = user;

			res.json({
				success: 1,
				user
			});
		} else {
			res.json({
				success: 0,
				user
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.signatureSave = async (req, res) => {
	try {
		const img 			= req.body.dataURL;
		const data 			= img.replace(/^data:image\/\w+;base64,/, '');
		const buf 			= Buffer.from(data, 'base64');
		const filename		= req.session.application._id+Date.now()+ '.png'
		const signatureName = 'src/modules/public/users/images/signatures/' + filename;
		fs.writeFile(signatureName, buf, function (err) {
			if (err) {
				res.json({
					success: 0
				});
			}
		});

		const updateUser = await User.findByIdAndUpdate(
			req.session.application._id, {
				signatureName: process.env.MEDIAURL + '/users/images/signatures/' + filename
			}, {
				new: true,
			}
		);
		req.session.application = updateUser;

		res.json({
			success: 1,
			img: filename
		});
	} catch (err) {
		console.log(err);
	}
};

exports.postRegisterDetail11 = async (req, res) => {
	try {
		req.body.accountTerms =
			typeof req.body.accountTerms !== 'undefined' ?
			req.body.accountTerms :
			null;
		req.body.riskDis =
			typeof req.body.riskDis !== 'undefined' ? req.body.riskDis : null;
		req.body.pennyStocks =
			typeof req.body.pennyStocks !== 'undefined' ? req.body.pennyStocks : null;
		req.body.electronicAccess =
			typeof req.body.electronicAccess !== 'undefined' ?
			req.body.electronicAccess :
			null;
		req.body.marginDisclosure =
			typeof req.body.marginDisclosure !== 'undefined' ?
			req.body.marginDisclosure :
			null;
		req.body.w9_Certification =
			typeof req.body.w9_Certification !== 'undefined' ?
			req.body.w9_Certification :
			null;
		req.body.stock_Locate =
			typeof req.body.stock_Locate !== 'undefined' ?
			req.body.stock_Locate :
			null;

		req.body.marginDisc =
			typeof req.body.marginDisc !== 'undefined' ? req.body.marginDisc : null;

		req.body.confirmedElectronic =
			typeof req.body.confirmedElectronic !== 'undefined' ?
			req.body.confirmedElectronic :
			false;

		req.body.isDraft = true;
		const updateUser = await User.findByIdAndUpdate(req.body.id, req.body, {
			upsert: true,
		});

		const user = await User.findOne({
			email: updateUser.email
		});

		let userAudit = await Audit.updateOne({
			userId: req.body.id
		}, {
			'formSubmitted.type': 'Form Submisson',
			'formSubmitted.date': Date.now(),
		}, {
			new: true
		});

		if (updateUser) {
			let binary_fileD;
			req.session.application = user;
			res.render(
				'pdf/index', {
					application: req.session.application
				},
				(err, data) => {
					if (err) {
						console.log(err);
					} else {
						pdfshift
							.prepare(data)
							.convert()
							.then(async function (binary_file) {
								// fs.writeFile(
								//   'src/modules/public/users/pdf/wos' +
								//     req.session.application._id +
								//     '.pdf',
								//   binary_file,
								//   'binary',
								//   function () {}
								// );
								// binary_fileD = await binary_file;

								await fs.writeFile(
									'src/modules/public/users/pdf/ws' +
									req.session.application._id +
									'.pdf',
									binary_file,
									'binary',
									function () {}
								);
							})
							.catch(function ({
								message,
								code,
								response,
								errors
							}) {
								console.log(response);
							});
					}
				}
			);
			req.session.pdf = 'ws' + req.session.application._id + '.pdf';
			console.log(binary_fileD);

			var isAddedSuccessfully = await AccountOpenignAPIController.callAccountOpenignAPI(user._id);
			console.log("isAddedSuccessfully", isAddedSuccessfully);
			if(isAddedSuccessfully) {
				res.json({
					success: 1,
					user,
					//     pdfBinary: binary_fileD,
				});
			}
			else{
				res.json({
					success: 0,
					user,
					message: "Unable to process the request at this time, please try again"
				});
			}
		} else {
			res.json({
				success: 0,
				user,
				message: "No user exists"
			});
		}
	} catch (err) {
		console.log(err);
	}
};

exports.logout = async (req, res) => {
	req.session.destroy();
	res.redirect('/login');
};

const addBaseUrlWithImagePath = (input) => {
	for(var i = 0; i < input.photo.length; i++){
		if(input.photo[i] && !input.photo[i].includes(process.env.MEDIAURL)){
			input.photo[i] = process.env.MEDIAURL + input.photo[i];
		}
	}
	return input;
}
